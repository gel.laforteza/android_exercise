package com.example.angelbert_laforteza_android_app

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.angelbert_laforteza_android_app.databinding

class RecyclerViewAdapter (
    private val list: List<String>,
    private val onClickListener: (String, Int) -> Unit
    ) : RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>(){

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val binding = ItemDataBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return RecyclerView.ViewHolder
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.textViewDataTask.text = list[position]
        holder.binding.constraintLayoutItemTaskContainer.setOnclickListener{
            onClickListener(list[position, position)
        }
    }

    override fun getItemCount() = list.size


    class ViewHolder(val binding: ItemDataBinding) :RecyclerView.ViewHolder(binding.root)
}