package com.example.angelbert_laforteza_android_app

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import com.example.angelbert_laforteza_android_app.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private var listTask: MutableList<String> = mutableListOf()

    private var x : String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        binding = ActivityMainBinding.inflate(layoutInflater)

        init()
    }

    private fun init(){
        supportActionBar?.title = "Guest Information"

        binding.buttonGuestInfomation.setOnClickListener{

            if (listTask.isNullOrEmpty()){
                setupRecylclerView(
                    binding.editTextFullname.text.toString(),
                    binding.editTextNumber.text.toString(),
                    binding.editTextAddress.text.toString())
            }else{
                addData(
                    binding.editTextFullname.text.toString(),
                    binding.editTextNumber.text.toString(),
                    binding.editTextAddress.text.toString())
            }

            binding.editTextFullname.setText("")
            binding.editTextNumber.setText("")
            binding.editTextAddress.setText("")
        }
    }


    private fun setupRecylclerView(data: String){
        listTask.add(data)
        binding.recyclerView
    }

    private fun addData(data: String){
        listTask.add(data)
        binding.recyclerViewTask
    }
}